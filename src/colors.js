export const primary = '#22b8cf';
export const primaryLighter = '#99e9f2';

export const background = '#212529';

export const white = '#ffffff';
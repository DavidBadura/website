import React from 'react';
import styled from "@emotion/styled";

function Background() {
  return (
    <Svg>
      <Rect x={280} y={0} width={120} height={40} color="rgba(34, 184, 207, .5)"/>
      <Rect x={320} y={0} width={80} height={40} color="rgba(34, 184, 207, .5)"/>
      <Rect x={340} y={0} width={60} height={80} color="rgba(34, 184, 207, .5)"/>
      <Rect x={360} y={0} width={40} height={100} color="rgba(34, 184, 207, .5)"/>
      <Rect x={360} y={0} width={40} height={40} color="rgba(34, 184, 207, 1)"/>

      <Rect x={0} y={190} width={120} height={40} color="rgba(34, 184, 207, .5)"/>
      <Rect x={0} y={190} width={80} height={40} color="rgba(34, 184, 207, .5)"/>
      <Rect x={0} y={170} width={60} height={60} color="rgba(34, 184, 207, .5)"/>
      <Rect x={0} y={150} width={40} height={80} color="rgba(34, 184, 207, .5)"/>
      <Rect x={0} y={190} width={40} height={40} color="rgba(34, 184, 207, 1)"/>
    </Svg>
  );
}

function Rect({x = 0, y = 0, width = 20, height = 20, color = "none", stroke = "none", strokeWidth = 1}) {
  return (
    <rect x={x} y={y} width={width} height={height} style={{
      fill: color,
      stroke: stroke,
      strokeWidth: strokeWidth
    }}/>
  );
}

export default Background;

const Svg = styled.svg`
  z-index: 0;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

import React from 'react';

const Icon = ({name}) => <i className={'fa fa-' + name} aria-hidden="true"/>;

export default Icon;
